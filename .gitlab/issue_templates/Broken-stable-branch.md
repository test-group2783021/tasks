<!---

Incident issues for failures on GitLab stable branches are automatically created by a GitLab script,
see https://gitlab.com/gitlab-org/gitlab/-/blob/master/scripts/create-pipeline-failure-incident.rb.

This template can be manually used in case the automation is not available.
-->

**Branch: {+TODO+}**

**Commit: {+TODO+}**

**Merge Request: {+TODO+}**

**Failed jobs: {+TODO+}**

### General guidelines

A broken stable branch prevents patch releases from being built.
Fixing the pipeline is a priority to prevent any delays in releases.

The process in the [Broken `master` handbook guide](https://about.gitlab.com/handbook/engineering/workflow/#broken-master) can be referenced since much of that process also applies here.

### Investigation

**Be sure to fill the `Timeline` for this incident.**

1. If the failure is new, and looks like a potential flaky failure, you can retry the failing job.
  Make sure to mention the retry in the `Timeline` and leave a link to the retried job.
1. Search for similar master-broken issues in https://gitlab.com/gitlab-org/quality/engineering-productivity/master-broken-incidents/-/issues
  1. If one exists, ask the DRI of the master-broken issue to cherry-pick any resulting merge requests into the stable branch

@gitlab-org/release/managers if the merge request author or maintainer is not available, this can be escalated using the dev-on-call process in the [#dev-escalation slack channel](https://gitlab.slack.com/archives/CLKLMSUR4).

### Pre-resolution

If you believe that there's an easy resolution by either:

- Reverting a particular merge request.
- Making a quick fix (for example, one line or a few similar simple changes in a few lines).
  You can create a merge request, assign to any available maintainer, and ping people that were involved/related to the introduction of the failure.
  Additionally, a message can be posted in `#backend_maintainers` or `#frontend_maintainers` to get a maintainer take a look at the fix ASAP.
- Cherry picking a change that was used to fix a similar master-broken issue.

### Resolution

Add a comment to this issue describing how this incident could have been prevented earlier in the Merge Request pipeline (rather than the merge commit pipeline).

/label ~release-blocker
