## Departing team member

Team Member: `email`, `GitLab handle`

 - [ ] Assign this issue to a Delivery Group EM

## Manager offboarding tasks

For all team members:

- [ ] Remove team member from the delivery-team@gitlab.com Google group
- [ ] Remove team member from [Delivery group](https://gitlab.com/groups/gitlab-org/delivery) on GitLab.com
- [ ] Remove team member from [Ops Delivery group](https://ops.gitlab.net/groups/gitlab-com/delivery/-/group_members)
- [ ] Remove team member from Chatops Slack app: https://app.slack.com/app-settings/T02592416/A04P4TD0GKH/collaborators
- [ ] Remove team member from Release-Tools app: https://app.slack.com/app-settings/T02592416/A0385PGTMSQ/collaborators
- [ ] Update Delivery Group handbook page 

Additional steps for Manager offboarding:

- [ ] Remove team member from [Delivery EMs group](https://gitlab.com/gitlab-org/delivery/managers) on GitLab.com
- [ ] Update "Maintained by" details for Delivery-owned Handbook pages