<!--
# BACKPORTING REQUEST

GitLab follows a strict maintenance policy. Before proceeding, please read and understand
https://docs.gitlab.com/ee/policy/maintenance.html and https://docs.gitlab.com/ee/policy/maintenance.html#backporting-to-older-releases

This template should be used to report *bug fixes* that should be considered for
backporting up to two releases. It is not required for security fixes.

The best way to ensure that your request is going to be considered is by
filling out all sections of the template appropriately.
-->

<!-- SET THE RIGHT LABELS (let the autocomplete guide you). YOU MUST SET SEVERITY AND PRIORITY LABELS -->
/label ~"devops:: ~"group:: ~"S ~"P ~backport
<!-- Assign this issue to an Engineer that Release Managers can utilize for assistance as necessary -->
/assign @

### Issue link

### Does this request relate to a bug or to a feature?
<!-- Note that under our current policy we cannot backport any features for the reasons listed in https://gitlab.com/gitlab-org/gitlab/-/blob/master/doc/policy/maintenance.md#patch-releases -->

### MR(s)

<!-- For each MR required for the backport, indicate whether or not it picks cleanly into the appropriate
stable branch, and/or whether or not a newly created MR has been merged in appropriately -->

- [ ] The same changes are already deployed to GitLab.com, and those MRs can be found in the Related Merge Requests table.

| MRs | Does this cleanly apply to the desired branch? | Is the MR ready for merge? | Notes |
| --- | ---------------------------------------------- | -------------------------- | ----- |
| ... | <!-- :heavy_check_mark::x: --> | <!-- :heavy_check_mark::x: --> | |

### Backport Versions

<!--
For each version we want to backport, add it to the table.

If referencing versions more than two releases old, then be specific about
whether your request is only to include a fix, should a patch release be
created. For example, in the Notes field: "This is not a request to create a
patch release for this version for this fix. This is only a request to include
the fix, should a patch release be created."
-->

:warning: Product Manager Approval needs to be provided in the table below for each version. Without Product Manager Approval, the Backport Request will not be taken into consideration by Release Managers :warning:

| Version | Approval from Product (to confirm the bug justifies the upgrade cost) | Approval by Release Manager | Notes |
| ------- | --------------------- | --------------------------- | ----- |
| ... | <!-- :heavy_check_mark::x: --> | <!-- :heavy_check_mark::x: --> | |


### Does this bug potentially result in data loss?

<!--
DO NOT NAME CUSTOMERS IN THIS ISSUE. LINK TO SALESFORCE OR ZENDESK
You must describe *why* affected customers are not able to upgrade to the latest
version of GitLab. Provide summarized information of customer account size, strategic importance etc.
The context provided here should allow the Release Manager to understand the *impact* of the bug.
Be as brief as possible.
 -->

### Customer impact

<!-- Add your stage/section's PM here and get them to provide context on the customer impact -->

**Product DRI - `@PM-Handle`**

<!--
We only currently provide patches to the current version of GitLab in accordance with our maintenance policy (https://docs.gitlab.com/ee/policy/maintenance.html). Anything outside of this is essentially unplanned and not officially supported by the Delivery Group.

That said, we all want the best outcome for customers and exceptions may be granted. The product addition here seeks to surface what the impact to users/customers is and if possible the dollar cost to GitLab of not backporting the change. This can help the Release Managers and the Delivery group to prioritize backports over the teams' other work if required.

Please explain the impact to customers, such as the risk of not backporting, and how many customers are expected to be impacted at this time.
-->


### Workaround

<!--
Please explain how one may be able to work around the problems, should the
decision be made to not backport the changes.
-->

/confidential

<!-- Mention the current release managers for review. See https://about.gitlab.com/community/release-managers/ -->
@gitlab-org/release/managers please assign yourselves to this issue.
